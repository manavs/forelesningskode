def mysort(liste):
    for i in range(1,len(liste)):          # range gaar til L-1
        element = liste[i]
        hull = i
        while hull > 0 and liste[hull-1] > element:
            liste[hull] = liste[hull-1]
            hull = hull - 1
            
        liste[hull] = element
    return liste

print(mysort([2,5,7,3]))

A=['Petter','Alex','Diana','Bodil','Anne']
B=mysort(A)
print(B)
