def min_funksjon(nokka):
    print(nokka[::-1])

# For bruk av denne, se ukjent_antall_parametre.py
def legg_sammen(*args):
    sum = 0
    for tall in args:
        sum += tall
    return sum
    
'''
(Dette om __main_ er nytt av året, og kommer ikke på eksamen)
if-sjekken under her gjør at funksjonskallet til min_funksjon 
kun kjøres når filen min_modul kjøres, og ikke når den importeres.
Du kan lese mer om hvorfor, blant annet her:
https://docs.python.org/3/library/__main__.html
https://stackoverflow.com/questions/419163/what-does-if-name-main-do
'''
if __name__ == '__main__':    
    print(min_funksjon("abcdefgh"))