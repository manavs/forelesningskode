filnavn = input('Oppgi filnavn: ')
try:
    f = open(filnavn,'r')
    soker = input('Oppgi tegn det søkes etter: ')
    posisjon = 0
    tegn = f.read(1) # Leser et tegn fra fila
    posListe = []
    while tegn:
        if tegn == soker:
            posListe.append(posisjon)
        tegn = f.read(1)    #Leser neste tegn
        posisjon += 1
    f.close()
    if len(posListe) != 0:
        print(f'Tegnet {soker} ble funnet i følgende posisjoner {posListe}')
    else:
        print(f'Tegnet {soker} ble ikke funnet i fila "{filnavn}"')
except IOError:
    print(f'Fila {filnavn} finnes ikke.')