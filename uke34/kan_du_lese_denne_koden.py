# Alt som står etter en skigard # kalles
# kommentarer, og blir ignorert av tolkeren.

# Koden ligger foran pensum. Frykt ikke.
# Dette ble bare laget for å vise dere
# hvor enkelt det er å forstå python.

# Den eneste snodige tingen er at en må
# gjøre om det som leses inn fra
# tastaturet til tall, det er jo teksten '4'...
# Herav int(), som prøver å tallifisere det.
# Hva skjer hvis du skriver inn bokstaver?

print("Velkommen til Plus'R'Us")
x = input('Skriv inn et tall: ') 
y = input('Skriv inn et tall: ')

print(x,'pluss', y, 'blir', int(x)+int(y))
# Nå for tiden skriver vi det gjerne slik:
print(f'{x} pluss {y} blir {int(x)+int(y)}')