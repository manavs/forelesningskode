import random

# 100 tilfeldige tall 0 - 100 inn i en liste:
tall = [random.randint(0,100) for i in range(100)]

print("Antall tall:",len(tall))
tall.sort()
print("Sorterte tall:", tall)

# Kjører du dette mange ganger, ser du at ulike tall trekkes.
# Og noen flere ganger. Men det finnes en måte å bare få unike
# verdier.
print("Set:", set(tall))
print("Antall elementer:", len(set(tall)))
